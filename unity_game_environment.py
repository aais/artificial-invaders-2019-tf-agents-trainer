import os
import json
import random
import numpy as np
from tf_agents.environments import py_environment
from tf_agents.specs import array_spec
from tf_agents.trajectories import time_step

from unity_game import UnityGame


class UnityGameEnvironment(py_environment.PyEnvironment):
    def __init__(self, shared_training_status, game_env_path, reset_params_file, training, play_level=None):
        super().__init__()
        self._shared_training_status = shared_training_status
        self._training = training
        game, batch_size = self.configure_ai(game_env_path)
        self._game = game
        self._batch_size = batch_size
        self._num_actions = self._game.get_num_of_actions()
        self._num_observations = self._game.get_num_of_observations()
        self._level = 0

        self._action_spec = array_spec.BoundedArraySpec(shape=(), dtype=np.int32, minimum=0, maximum=self._num_actions - 1, name='action')
        self._observation_spec = array_spec.BoundedArraySpec(shape=(self._num_observations,), dtype=np.float64, minimum=0, maximum=1, name='observation')
        
        with open(reset_params_file, 'r') as file:
            json_string = file.read()
            self._unity_scene_reset_parameters = json.loads(json_string)
        
        if training == True:
            self._current_level = -1
        else:
            if play_level is None:
                raise Exception("Level to play has to be specified")
            self._current_level = play_level
        
        self._max_level = len(self._unity_scene_reset_parameters["thresholds"])


    # @staticmethod
    def configure_ai(self, game_env_path):
        game = UnityGame(game_env_path)
        batch_size = game.init_game()
        return game, batch_size


    @property
    def batched(self):
        return True


    def batch_size(self):
        return self._batch_size


    def action_spec(self):
        return self._action_spec


    def observation_spec(self):
        return self._observation_spec


    def _reset(self):
        reset_params = self._create_reset_params(
            self._unity_scene_reset_parameters, self._shared_training_status,
            self._current_level, self._max_level, self._training)
        if reset_params is not None:
            self._current_level += 1

        self._game.reset(reset_params)
        time_steps = [None] * self._batch_size
        observations = self._game.get_observations()
        for i in range(len(time_steps)):
            time_steps[i] = time_step.restart(observations[i])
        return time_steps


    def _step(self, actions):
        if not isinstance(actions, list):
            actions = [[actions]]
        time_steps = [None] * self._batch_size
        previousEpisodes_finished =  self._game.are_episodes_finished()
        if len(time_steps) is not len(previousEpisodes_finished):
            raise ValueError(
                "time_steps and episodes_finished lists length is not the same"
                "time_steps length: {} vs. episodes_finished length {}".format(len(time_steps), len(previousEpisodes_finished)))

        for i in range(len(time_steps)):
            if previousEpisodes_finished[i] is True:
                actions[i] = 0 #Don't make an action in Unity

        rewards = self._game.make_action(actions)
        observations = self._game.get_observations()
        newEpisodes_finished =  self._game.are_episodes_finished()
        for i in range(len(time_steps)):
            if previousEpisodes_finished[i] == True:
                time_steps[i] = time_step.restart(observations[i])
            elif newEpisodes_finished[i] == True:
                time_steps[i] = time_step.termination(observations[i], rewards[i])
            elif newEpisodes_finished[i] == False:
                time_steps[i] = time_step.transition(observations[i], rewards[i])
            else:
                raise ValueError(
                "We shouldn't be here. Something wrong with episodes finished/not finished")
        return time_steps


    def close(self):
        if self._game is not None:
            self._game.close()
    

    def _create_reset_params(self, game_reset_params, training_status, current_level, max_level, training):
        reset_params = None
        if (training is True and game_reset_params is not None and game_reset_params["measure"] == "reward"):
            if (
                training_status["AverageReturn"] > game_reset_params["thresholds"][current_level] or
                current_level == -1):
                if current_level < max_level:
                    current_level += 1
                    print("### Loading new level: {}".format(current_level + 1))
                    reset_params = {}
                    for key in game_reset_params["parameters"].keys():
                        reset_params[key] = game_reset_params["parameters"][key][current_level]
                    print("### Parameters are: {}".format(reset_params))
        elif training is False:
            reset_params = {}
            for key in game_reset_params["parameters"].keys():
                reset_params[key] = game_reset_params["parameters"][key][current_level]
        
        return reset_params