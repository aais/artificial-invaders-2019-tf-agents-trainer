import datetime
from ruamel.yaml import YAML


def env_gen(env, shared_training_status, game_env_path, reset_params_file, training):
    return lambda: env(shared_training_status, game_env_path, reset_params_file, training)


def env_array_gen(number_of_envs, parent_env, child_env, shared_training_status, game_env_path, reset_params_file, training):
    return [lambda: parent_env(env_gen(child_env, shared_training_status, game_env_path, reset_params_file, training))] * number_of_envs

def log_exit_time(start_time):
    stop_time = datetime.datetime.now()
    print("============")
    print("Training started: {}".format(start_time))
    print("============")

    print("============")
    print("Training ended: {}".format(stop_time))
    print("============")

    print("============")
    print("Training duration: {}".format(stop_time - start_time))
    print("============")


def parse_options(path_to_options_file):
    with open(path_to_options_file, 'r') as f:
        yaml = YAML()
        options = yaml.load(f)
    return options