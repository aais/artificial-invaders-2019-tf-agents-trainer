## Copy "ml-agents-envs"-folder
Copy "ml-agents-envs"-folder to this projects root-folder from Unity3D ml agents repo: https://github.com/Unity-Technologies/ml-agents
Tested with commit 67ab5d412889d68480f3555eb634b292ad822be8 on the Unity ml agents repo.

## Create Python virtual environment if you want
### Ubuntu
`virtualenv venv`
`source venv/bin/activate`
### Windows 10
`python -m venv venv`
`venv\Scripts\Activate`

## Install Python packages
`pip install -r requirements-cpu.txt`
or 
`pip install -r requirements-gpu.txt`

## Build the Unity Artificial invader simulator project
- Build the Unity simulator from [here](https://gitlab.com/aais/artificial-invaders-2019-unity-simulator)
- Put the executable and the associated files to `training_env`-folder
- Copy the curricula `.json` file from `artificial-invaders-2019-unity-simulator`-repo's folder `/config/curricula/artificial-invader/` to the root of this repo

## Setup the training params
Open the `training-params.yaml` and set the variables to work for your environment.

## Run the training
`python unity_tf_agent.py`
