from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import time
import datetime
import random
import atexit

import tensorflow as tf
from absl import app
from absl import flags
from absl import logging
from tf_agents.agents.ppo import ppo_agent
from tf_agents.drivers import dynamic_episode_driver
from tf_agents.environments import tf_py_environment
from parallel_unity_py_environment import ParallelPyEnvironment
# from tf_agents.environments.parallel_py_environment import ParallelPyEnvironment
from unity_py_environment import UnityPyEnvironment
from tf_agents.eval import metric_utils
from tf_agents.metrics import tf_metrics
from tf_agents.networks.actor_distribution_rnn_network import ActorDistributionRnnNetwork
from tf_agents.networks.actor_distribution_network import ActorDistributionNetwork
from tf_agents.networks.value_rnn_network import ValueRnnNetwork
from tf_agents.networks.value_network import ValueNetwork
from tf_agents.replay_buffers import tf_uniform_replay_buffer
from tf_agents.utils import common
from multiprocessing import Manager, freeze_support

from unity_game_environment import UnityGameEnvironment
from utils import log_exit_time, env_gen, env_array_gen, parse_options


flags.DEFINE_string("training_params", "training-params.yaml", "Path to training params yaml-file", short_name="p")

FLAGS = flags.FLAGS


def create_networks(tf_env):
  actor_net = ActorDistributionNetwork(
      tf_env.observation_spec(),
      tf_env.action_spec(),
      fc_layer_params=(256, 256))
  value_net = ValueNetwork(
      tf_env.observation_spec(),
      fc_layer_params=(256, 256))
  
  return actor_net, value_net


def create_networks_rnn(tf_env):
  actor_net = ActorDistributionRnnNetwork(
    tf_env.observation_spec(),
    tf_env.action_spec(),
    # conv_layer_params=[(16, 8, 4), (32, 4, 2)],
    input_fc_layer_params=(256,),
    lstm_size=(64,),
    output_fc_layer_params=(256,),
    # activation_fn=tf.nn.elu
  )
  value_net = ValueRnnNetwork(
    tf_env.observation_spec(),
    # conv_layer_params=[(16, 8, 4), (32, 4, 2)],
    input_fc_layer_params=(256,),
    lstm_size=(64,),
    output_fc_layer_params=(256,),
    # activation_fn=tf.nn.elu
  )

  return actor_net, value_net


def train_eval(training_params):
  """A simple train and eval for PPO."""

  if training_params["model_path"] is None:
    raise AttributeError('Path to folder where model is saved is required.')
  if training_params["game_env"] is None:
    raise AttributeError('Path to game executable is required')

  shared_training_status = Manager().dict()
  shared_training_status["NumberOfEpisodes"] = -1
  shared_training_status["EnvironmentSteps"] = -1
  shared_training_status["AverageReturn"] = -1
  shared_training_status["AverageEpisodeLength"] = -1

  start_date = datetime.datetime.now()
  atexit.register(log_exit_time, start_date)

  eval_counter = training_params["eval_interval"]
  summary_counter = training_params["summary_interval"]
  log_counter = training_params["log_interval"]
  checkpoint_interval_counter = training_params["checkpoint_interval"]

  root_dir = os.path.expanduser(training_params["model_path"]) 
  train_dir = os.path.join(root_dir, 'train')
  eval_dir = os.path.join(root_dir, 'eval')

  num_parallel_environments = training_params["num_parallel_environments"] * training_params["num_agents_in_environment"]

  train_summary_writer = tf.compat.v2.summary.create_file_writer(train_dir, flush_millis=training_params["summaries_flush_secs"] * 1000)
  train_summary_writer.set_as_default()

  eval_summary_writer = tf.compat.v2.summary.create_file_writer(eval_dir, flush_millis=training_params["summaries_flush_secs"] * 1000)
  eval_metrics = [
    tf_metrics.AverageReturnMetric(buffer_size=training_params["num_eval_episodes"]),
    tf_metrics.AverageEpisodeLengthMetric(buffer_size=training_params["num_eval_episodes"])
  ]

  global_step = tf.compat.v1.train.get_or_create_global_step()
  with tf.compat.v2.summary.record_if(lambda: tf.math.equal(global_step > summary_counter, True)):
    # eval_tf_env  = tf_py_environment.TFPyEnvironment(
    #   UnityPyEnvironment(env_gen(
    #     UnityGameEnvironment, shared_training_status, training_params["game_env"], training_params["reset_params_file"], True))
    # )

    tf_env  = tf_py_environment.TFPyEnvironment(
      UnityPyEnvironment(env_gen(
        UnityGameEnvironment, shared_training_status, training_params["game_env"], training_params["reset_params_file"], True))
    )

    # TODO: How to get parallel py environments work?
    # tf_env  = tf_py_environment.TFPyEnvironment(
    #   ParallelPyEnvironment(
    #     env_array_gen(training_params["num_parallel_environments"], UnityPyEnvironment, UnityGameEnvironment,
    #       shared_training_status, training_params["game_env"], training_params["reset_params_file"], True)
    #   ))
    # tf_env  = tf_py_environment.TFPyEnvironment(
    #   ParallelPyEnvironment(
    #     [lambda: UnityPyEnvironment(env_gen(
    #       UnityGameEnvironment, shared_training_status, training_params["game_env"],
    #       training_params["reset_params_file"], True))] * training_params["num_parallel_environments"]
    # ))

    # THIS WORKS WITH ONE GAME, MULTIGAME HAS ISSUES WITH "Structure had 4 elements, but flat_sequence had 128 elements."
    # tf_env = tf_py_environment.TFPyEnvironment(
    #   ParallelPyEnvironment(
    #     [env_gen(UnityGameEnvironment, shared_training_status, training_params["game_env"], training_params["reset_params_file"], True)] * training_params["num_parallel_environments"],
    #     start_serially=False,
    #     flatten=True
    #   )
    # )

    # tf_env  = tf_py_environment.TFPyEnvironment(
    #   ParallelPyEnvironment(
    #     [lambda: UnityPyEnvironment(env_gen(
    #       UnityGameEnvironment, shared_training_status, training_params["game_env"],
    #       training_params["reset_params_file"], True))] * training_params["num_parallel_environments"]
    # ))

    optimizer = tf.compat.v1.train.AdamOptimizer(
      learning_rate=training_params["learning_rate"],
      epsilon=training_params["ppo_agent_options"]["optimizer_epsilon"])

    actor_net, value_net = create_networks(tf_env)

    tf_agent = ppo_agent.PPOAgent(
      tf_env.time_step_spec(),
      tf_env.action_spec(),
      optimizer,
      actor_net=actor_net,
      value_net=value_net,
      num_epochs=training_params["num_epochs"],
      train_step_counter=global_step,
      lambda_value=training_params["ppo_agent_options"]["lambda_value"],
      discount_factor=training_params["ppo_agent_options"]["discount_factor"],
      # gradient_clipping=0.5,
      entropy_regularization=training_params["ppo_agent_options"]["entropy_regularization"],
      # importance_ratio_clipping=0.2,
      use_gae=training_params["ppo_agent_options"]["use_gae"],
      use_td_lambda_return=training_params["ppo_agent_options"]["use_td_lambda_return"]
    )
    tf_agent.initialize()

    environment_steps_metric = tf_metrics.EnvironmentSteps()
    step_metrics = [
      tf_metrics.NumberOfEpisodes(),
      environment_steps_metric,
    ]

    train_metrics = step_metrics + [
      tf_metrics.AverageReturnMetric(batch_size=num_parallel_environments, buffer_size=128), # TODO: Make buffer_size to parameter
      tf_metrics.AverageEpisodeLengthMetric(batch_size=num_parallel_environments, buffer_size=128)
    ]

    eval_policy = tf_agent.policy
    collect_policy = tf_agent.collect_policy

    train_checkpointer = common.Checkpointer(
      ckpt_dir=os.path.join(train_dir, 'trainer'),
      agent=tf_agent,
      global_step=global_step,
      metrics=metric_utils.MetricsGroup(train_metrics, 'train_metrics'))
    policy_checkpointer = common.Checkpointer(
      ckpt_dir=os.path.join(train_dir, 'policy'),
      policy=eval_policy,
      global_step=global_step)

    replay_buffer = tf_uniform_replay_buffer.TFUniformReplayBuffer(
      tf_agent.collect_data_spec,
      batch_size=num_parallel_environments,
      max_length=training_params["replay_buffer_capacity"])

    collect_driver = dynamic_episode_driver.DynamicEpisodeDriver(
      tf_env,
      collect_policy,
      observers=[replay_buffer.add_batch] + train_metrics,
      num_episodes=training_params["collect_episodes_per_iteration"])


    def train_step():
      trajectories = replay_buffer.gather_all()
      return tf_agent.train(experience=trajectories)

    def print_metrics(metrics):
      shared_training_status["NumberOfEpisodes"] = metrics[0].result().numpy().item()
      shared_training_status["EnvironmentSteps"] = metrics[1].result().numpy().item()
      shared_training_status["AverageReturn"] = metrics[2].result().numpy().item()
      shared_training_status["AverageEpisodeLength"] = metrics[3].result().numpy().item()
      resultStr = ""
      for metric in metrics:
        resultStr += "|  {}: {}  |  ".format(metric.name, metric.result().numpy().item())
      print("Step: {} / num_environment_steps: {}".format(environment_steps_metric.result(), training_params["num_environment_steps"]))
      print(resultStr)

    def evaluate():
      print("======== ========== ========")
      print("======== Evaluating ========")
      print("======== ========== ========")
      # metric_utils.eager_compute(
      #   eval_metrics, eval_tf_env, eval_policy, training_params["num_eval_episodes"],
      #   global_step, eval_summary_writer, 'Metrics')
      print("======== ========== ========")

    def save_checkpoint(global_step_val):
      print("======== ====== ========= ========")
      print("======== Saving Checkpont ========")
      print("======== ====== ========= ========")
      train_checkpointer.save(global_step=global_step_val)
      policy_checkpointer.save(global_step=global_step_val)
      print("======== ======== ========")

    if training_params["use_tf_functions"]:
      # TODO(b/123828980): Enable once the cause for slowdown was identified.
      collect_driver.run = common.function(collect_driver.run, autograph=False)
      tf_agent.train = common.function(tf_agent.train, autograph=False)
      train_step = common.function(train_step)

    collect_time = 0
    train_time = 0
    timed_at_step = global_step.numpy()

    print("Starting running")
    while environment_steps_metric.result() < training_params["num_environment_steps"]:      
      start_time = time.time()
      collect_driver.run()
      collect_time += time.time() - start_time

      start_time = time.time()
      print("======== ======== ========")
      print("======== Training ========")
      print("======== ======== ========")
      total_loss, _ = train_step()
      replay_buffer.clear()
      train_time += time.time() - start_time

      for train_metric in train_metrics:
        train_metric.tf_summaries(train_step=global_step, step_metrics=train_metrics)#step_metrics)
      print_metrics(train_metrics)

      global_step_val = global_step.numpy()

      print("global_step_val: {}".format(global_step_val))
      
      if global_step_val > log_counter:
        log_counter += training_params["log_interval"]
        print("======== ======= ========")
        print("======== Logging ========")
        print("======== ======= ========")
        logging.info('step = %d, loss = %f', global_step_val, total_loss)
        steps_per_sec = ((global_step_val - timed_at_step) / (collect_time + train_time))
        logging.info('%.3f steps/sec', steps_per_sec)
        logging.info('collect_time = {}, train_time = {}'.format(collect_time, train_time))
        with tf.compat.v2.summary.record_if(True):
          tf.compat.v2.summary.scalar(name='global_steps_per_sec', data=steps_per_sec, step=global_step)

        timed_at_step = global_step_val
        collect_time = 0
        train_time = 0
        print("======== ======= ========")

      if global_step_val > eval_counter:
        eval_counter += training_params["eval_interval"]
        evaluate()

      if global_step_val > checkpoint_interval_counter:
        checkpoint_interval_counter += training_params["checkpoint_interval"]
        save_checkpoint(global_step_val)

    # One final eval before exiting.
    evaluate()
      
  print("result: {},num_environment_steps: {}".format(environment_steps_metric.result(), training_params["num_environment_steps"]))

  print("======== ======= ========")
  print("======== Exiting ========")
  print("======== ======= ========")

def start(_):
  tf.compat.v1.enable_v2_behavior()  # For TF 1.x users
  logging.set_verbosity(logging.INFO)
  training_params = parse_options(FLAGS.training_params)
  train_eval(training_params)

if __name__ == '__main__':
  flags.mark_flag_as_required('training_params')
  app.run(start)

